import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import svgr from 'vite-plugin-svgr';
import mkcert from 'vite-plugin-mkcert';
export default defineConfig({
    plugins: [svgr({ exportAsDefault: true }), react(), mkcert()],
    resolve: {
        alias: [{ find: '@', replacement: '/src' }],
    },
    server: {
        https: true,
        proxy: {
            '/api': {
                target: 'https://localhost:62145/',
                changeOrigin: true,
                secure: false,
            },
        },
    },
});
