import { configureStore } from "@reduxjs/toolkit";
// import configReducer from "../features/configSlice";
// import adminReducer from "../features/adminFormSlice";
// import { userReducer } from "../features/userSlice";
import { ThunkExtraArg } from "../interfaces/common";
import { $realApi } from "../services/api";
import { userReducer } from "@/features/selectors/userSlice";

const extraArg: ThunkExtraArg = {
  api: $realApi,
};

export const store = configureStore({
  reducer: {
    // config: configReducer,
    // admin: adminReducer,
    user: userReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
      thunk: {
        extraArgument: extraArg,
      },
    }),
});

export type RootState = ReturnType<typeof store.getState>;
export type RootDispatch = typeof store.dispatch;
