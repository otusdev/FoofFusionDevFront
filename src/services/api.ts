import axios, { AxiosError } from "axios";
// import { USER_LOCALSTORAGE_EMAIL, USER_LOCALSTORAGE_REFRESH_TOKEN, USER_LOCALSTORAGE_SIGNED, USER_LOCALSTORAGE_TOKEN } from "../constants/constants";
import { ILoginResponse } from "../interfaces/common";
import { USER_LOCALSTORAGE_TOKEN, USER_LOCALSTORAGE_REFRESH_TOKEN, USER_LOCALSTORAGE_EMAIL, USER_LOCALSTORAGE_SIGNED } from "@/constants";

export const $realApi = axios.create({
    baseURL: '/api',
    withCredentials: true,
});

$realApi.interceptors.request.use((config) => {
    config.headers!.authorization = `Bearer ${localStorage.getItem(
        USER_LOCALSTORAGE_TOKEN,
    )}`;
    config.headers["site"] = window.location.href;
    return config;
});

$realApi.interceptors.response.use(
    (config) => config,
    async (error) => {
        const originalRequest = error.config;
        if (
            error.response.status === 401 &&
            error.config &&
            !error.config._isRetry
        ) {
            originalRequest._isRetry = true;
            try {
                const refreshToken = localStorage.getItem(
                    USER_LOCALSTORAGE_REFRESH_TOKEN,
                );
                const login = localStorage.getItem(USER_LOCALSTORAGE_EMAIL);
                const response = await axios.post<ILoginResponse>(
                    '/api/Auth/RefreshToken',
                    { login, refreshToken },
                );
                localStorage.setItem(
                    USER_LOCALSTORAGE_TOKEN,
                    response.data.body.accessToken,
                );
                localStorage.setItem(
                    USER_LOCALSTORAGE_REFRESH_TOKEN,
                    response.data.body.refreshToken,
                );
                return $realApi.request(originalRequest);
            } catch (e) {
                const typedError = e as AxiosError;
                const isNotAuthorizedStatus = typedError.response?.status;
                if (
                    isNotAuthorizedStatus === 403 ||
                    isNotAuthorizedStatus === 500
                ) {
                    localStorage.removeItem(USER_LOCALSTORAGE_SIGNED);
                    window.location.reload();
                }
            }
        }
        throw error;
    },
);
