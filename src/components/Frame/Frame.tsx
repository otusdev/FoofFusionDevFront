import React, { useState } from 'react';
import { LaptopOutlined, MenuFoldOutlined, MenuUnfoldOutlined, NotificationOutlined, SearchOutlined, UserOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Avatar, Badge, Breadcrumb, Button, Input, Layout, Menu, theme } from 'antd';
import Brand from '../Brand';
// import Header1 from '../Header';
import { useSelector } from 'react-redux';
import { getCurrentUser, getUserAuth } from '@/features/selectors/currentUser';
// import localStyles from "./Users.module.scss";
import Anonim from '../../images/Anonim.png';
// import { avatar } from '@/pages/Users/styled';
// import './avatarStyle.css';
// import { LayoutsProps } from '@/interfaces/common';
// import { renderMenuItems } from '@/Config';
import { Outlet } from 'react-router-dom';
import { LayoutsProps } from '@/interfaces/common';
const { Header, Content, Sider } = Layout;

// const items1: MenuProps['items'] = ['1', '2', '3'].map((key) => ({
//     key,
//     label: `nav ${key}`,
// }));

// const items2: MenuProps['items'] = [UserOutlined, LaptopOutlined, NotificationOutlined].map(
//     (icon, index) => {
//         const key = String(index + 1);

//         return {
//             key: `sub${key}`,
//             icon: React.createElement(icon),
//             label: `subnav ${key}`,
//             children: new Array(4).fill(null).map((_, j) => {
//                 const subKey = index * 4 + j + 1;
//                 return {
//                     key: subKey,
//                     label: `option${subKey}`,
//                 };
//             }),
//         };
//     },
// );

const Frame = (props: any) => {
    const { menuItems } = props;
    const isAuth = useSelector(getUserAuth);
    const currentUser = useSelector(getCurrentUser);
    const [message, setMessage] = useState<number>(1);
    const [collapsed, setCollapsed] = useState(false);
    const { token: { colorBgContainer, borderRadiusLG } } = theme.useToken();
    const toggleCollapsed = () => {
        setCollapsed(!collapsed);
    };
    return (
        <Layout>
            <Header style={{ display: 'flex', alignItems: 'left', backgroundColor: "white", paddingLeft: 0, height: "46px", background: "#1675e0" }}>
                {/* <div className="logo" /> */}
                {/* <Brand /> */}
                <Button size='large' type="primary" onClick={toggleCollapsed} style={{ marginBottom: 16, height: "100%", background: "#1675e0" }}>
                    {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                </Button>
                {/* <Menu
                    theme="light"
                    mode="horizontal"
                    defaultSelectedKeys={['2']}
                    items={items1}
                    style={{ flex: 1, minWidth: 0 }}
                /> */}
                <div style={{ width: "25%" }}> </div>
                <Input prefix={<SearchOutlined />} placeholder="Search" style={{ width: "25%", marginLeft: 'auto', margin: 10 }} /> {/* Встроенная поисковая строка */}
                <div style={{ marginLeft: 'auto', display: 'flex' }}>
                    <Badge count={message} offset={[0, 30]}>
                        <Avatar className='ant-avatar' src={currentUser.picture == 'undefined' || currentUser.picture == null ? Anonim : `${currentUser.picture}`} /> {/* Замените ссылку на реальный URL вашей аватарки пользователя */}
                    </Badge>
                </div>
            </Header>
            <Layout>
                <Sider width={200} style={{ background: colorBgContainer }}
                    collapsed={collapsed}>
                    {/* <Menu mode="inline"  defaultSelectedKeys={['1']} style={{ height: '100%', borderRight: 0 }}>
                        {renderMenuItems(menuItems)}                        
                    </Menu> */}

                    <Menu                       
                        mode="inline"
                        defaultSelectedKeys={['1']}
                        defaultOpenKeys={['Settings','Recycle']}
                        style={{ height: '100%', borderRight: 0 }}                       
                        items={menuItems}
                        // inlineCollapsed={true}
                    />
                </Sider>
                <Layout style={{ padding: '0 24px 24px' }}>
                    <Breadcrumb style={{ margin: '16px 0' }}
                    // items=["Home",]
                    />
                        {/* <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item> */}
                   
                    <Content
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                            background: colorBgContainer,
                            borderRadius: borderRadiusLG,
                        }}
                    >
                        {/* Content */}
                        <Outlet />
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
};
export default Frame;