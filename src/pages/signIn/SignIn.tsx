import React, { useCallback, useState } from 'react';
import { Form, Button, Panel, IconButton, Stack, Divider } from 'rsuite';
import { Link, useNavigate } from 'react-router-dom';
import GithubIcon from '@rsuite/icons/legacy/Github';
import FacebookIcon from '@rsuite/icons/legacy/Facebook';
import GoogleIcon from '@rsuite/icons/legacy/Google';
import WechatIcon from '@rsuite/icons/legacy/Wechat';
import { useAppDispatch } from '@/app/hooks';
import { loginByUsername } from '@/features/selectors/loginByUsername';
// import Link from 'antd/es/typography/Link';
// import Brand from '../../../components/Brand'
// import { AuthService } from '../../../services/auth/auth.service';
// import { UserService } from '../../../services/auth/user.service';
// import { loginByUsername } from './services/loginByUsername';
// import { useAppDispatch } from '../../../app/hooks';
// export let authService: AuthService = new AuthService();
// export let userService: UserService = new UserService();
export interface FormValue {
  username: "",
  password: ""
}

const SignIn = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [loginInfo, setloginInfo] = useState<any>({
    formValue: {
      username: "",
      password: "",
      isInternalUser: false
    }
  })
  //   const handlePostForm = (e: React.FormEvent) => {
  //     console.log(selectedItems);
  // console.log(e);
  //   }
  const login = loginInfo.login;
  const password =loginInfo.password;
  const isInternalUser =  false;//loginInfo.isInternalUser;
  const handleAuthSend = useCallback(async () => {
    // if (handleValidation()) {
    //   void dispatch(fetchSaveUpdateAip(selectedItems, itemId));
    //   toggleShowModal();    
    const result = await dispatch(loginByUsername({ login, password, isInternalUser }));
    if (result.meta.requestStatus === 'fulfilled') {
      // navigate(getRouteHome())
      navigate("/profile");;
    }
  }, [dispatch, loginInfo.password, loginInfo.login, navigate, isInternalUser]);
  console.log('handleSave');
  // userService.getList();
  //--
  // authService.login(loginInfo.login, loginInfo.password, false).then(
  //   () => {
  //     // userService.getList();
  //     navigate("/profile");
  //     //window.location.reload();
  //   },
  //   (error) => {
  //     const resMessage =
  //       (error.response &&
  //         error.response.data &&
  //         error.response.data.message) ||
  //       error.message ||
  //       error.toString();
  //   }
  //   )
  //--
  //}


  return (
    <Stack
      justifyContent="center"
      alignItems="center"
      direction="column"
      style={{
        height: '100vh'
      }}
    >
      {/* <Brand style={{ marginBottom: 10 }} /> */}

      <Panel bordered style={{ background: '#fff', width: 400 }} header={<h3>Sign In</h3>}>
        <p style={{ marginBottom: 10 }}>
          <span className="text-muted">New Here? </span>{' '}
          <Link to="/sign-up"> Create an Account</Link>
        </p>

        <Form fluid onChange={(loginInfo: any) => { setloginInfo(loginInfo) }}>
          <Form.Group>
            <Form.ControlLabel>Login or email address</Form.ControlLabel>
            <Form.Control name="login" />
          </Form.Group>
          <Form.Group>
            <Form.ControlLabel>
              <span>Password</span>
              <a style={{ float: 'right' }}>Forgot password?</a>
            </Form.ControlLabel>
            <Form.Control name="password" type="password" />
          </Form.Group>
          <Form.Group>
            <Stack spacing={6} divider={<Divider vertical />}>
              <Button appearance="primary" onClick={handleAuthSend} type="submit">Sign in</Button>
              <Stack spacing={6}>
                <IconButton icon={<WechatIcon />} appearance="subtle" />
                <IconButton icon={<GithubIcon />} appearance="subtle" />
                <IconButton icon={<FacebookIcon />} appearance="subtle" />
                <IconButton icon={<GoogleIcon />} appearance="subtle" />
              </Stack>
            </Stack>
          </Form.Group>
        </Form>
      </Panel>
    </Stack>
  );
};

export default SignIn;
